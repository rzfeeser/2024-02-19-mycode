#!/usr/bin/python

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = r'''
---
module: custom_nasa_apod

short_description: This interfaces with nasa APOD URI

# If this is part of a collection, you need to use semantic versioning,
# i.e. the version is of the form "2.5.0" and not "2.4".
version_added: "1.0.0"

description: This is my longer description explaining my test module.

options:
    date:
        description: This is the date to lookup
        required: false
        type: str
    api_key:
        description: API key to https://api.nasa.gov/. Defaults to DEMO_KEY
        required: false
        type: str
# Specify this value according to your collection
# in format of namespace.collection.doc_fragment_name
# extends_documentation_fragment:
#     - my_namespace.my_collection.my_doc_fragment_name

author:
    - rzfeeser (@yourGitHubHandle)
'''

EXAMPLES = r'''
# return 2024-01-02 NASA APOD
- name: return 2024-01-02 nasa apod
  custom_nasa_apod:
    date: 2024-01-02
    api_key: abcdef_nasa_key_123456

# return todays nasa apod
- name: return todays nasa apod
  custom_nasa_apod:
'''

RETURN = r'''
# These are examples of possible return values, and in general should use other names for return values.
url_looked_up:
    description: HTTP message was sent to this url
    type: str
    returned: always
    sample: 'https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY'
http_response_code:
    description: This is the response code returned to the HTTP lookup
    type: int
    returned: always
    sample: 200
nasa_json:
    description: This is the data returned by nasa
    type: dict
    returned: always
    sample: see https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY
'''

from ansible.module_utils.basic import AnsibleModule
import requests


def run_module():
    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        date=dict(type='str', required=False),
        api_key=dict(type='str', required=False, default='DEMO_KEY')
    )

    # seed the result dict in the object
    # we primarily care about changed and state
    # changed is if this module effectively modified the target
    # state will include any data that you want your module to pass back
    # for consumption, for example, in a subsequent task
    result = dict(
        changed=False,
        http_response_code=0,
        url_looked_up='',
        nasa_json={}
    )

    # the AnsibleModule object will be our abstraction working with Ansible
    # this includes instantiation, a couple of common attr would be the
    # args/params passed to the execution, as well as if the module
    # supports check mode
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    # if the user is working with this module in only check mode we do not
    # want to make any changes to the environment, just return the current
    # state with no modifications
    if module.check_mode:
        module.exit_json(**result)

    # manipulate or modify the state as needed (this is going to be the
    # part where your module will do what it needs to do)
    # create our url to lookup
    url = 'https://api.nasa.gov/planetary/apod?'

    if module.params['date']:
        url += 'date=' + module.params['date']

    url += '&api_key=' + module.params['api_key']
    
    # in the JSON we are going to return, set the URL we are about to lookup
    result['url_looked_up'] = url

    

    # lookup the url
    nasa_lookup = requests.get(url)

    # check the response code
    result['http_response_code'] = nasa_lookup.status_code

    # return our JSON
    result['nasa_json'] = nasa_lookup.json()

    # during the execution of the module, if there is an exception or a
    # conditional state that effectively causes a failure, run
    # AnsibleModule.fail_json() to pass in the message and the result
    if result['http_response_code'] != 200:
        module.fail_json(msg='You received back a non-200 response code!', **result)

    # in the event of a successful module execution, you will want to
    # simple AnsibleModule.exit_json(), passing the key/value results
    module.exit_json(**result)


def main():
    run_module()


if __name__ == '__main__':
    main()
